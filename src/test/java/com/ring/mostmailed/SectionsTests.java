package com.ring.mostmailed;

import com.ring.BaseApiTest;
import enums.Section;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class SectionsTests extends BaseApiTest {

	private final Logger log = LoggerFactory.getLogger(SectionsTests.class);

	@Test(groups = {"P0", "Smoke", "Regression", "Sections"}, testName = "Sections Test",
			description = "Verify 'num_results' value")
	public void testSections() throws Exception {
		log.info("Step 1: Send request");
		Response response = given()
				.when()
				.get(SECTIONS_ENDPOINT);

		log.info("Step 2: Verify response code and status");
		response
				.then()
				.assertThat()
				.statusCode(200)
				.body("status", is("OK"));

		log.info("Step 3: Verify all sections");
		List<String> sectionsList = response
				.getBody()
				.jsonPath()
				.getList("results.name");

		List<String> listOfEnumValues = Arrays.stream(Section.values())
				.map(Section::getValue)
				.map(o -> o.equals("nyregion") ? "N.Y. / Region" : o)
				.collect(Collectors.toList());

		for (int i = 0; i < sectionsList.size(); i++) {
			Assert.assertTrue(listOfEnumValues.contains(sectionsList.get(i)),
					String.format("It's unknown section value: %s", sectionsList.get(i)));
		}
	}
}

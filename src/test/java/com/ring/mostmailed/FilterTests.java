package com.ring.mostmailed;

import com.ring.BaseApiTest;
import dto.Article;
import enums.Section;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static enums.Section.ALL_SECTIONS;
import static enums.TimePeriod.DAY;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class FilterTests extends BaseApiTest {

	private final Logger log = LoggerFactory.getLogger(FilterTests.class);

	private List<Article> totalArticles = new ArrayList<>();
	private static Set<Section> sections;
	private Map<Section, Long> articlesPerSection;

	@Test(groups = {"P0", "Smoke", "Regression", "Filter", "Most Mailed"}, testName = "Filter Test",
			description = "Get response for most emailed articles for all-sections.")
	public void getMostMailedArticlesTest() throws Exception {
		log.info("Step 1: Get response for most emailed articles for all-sections.");
		int pagesCounter = 0;
		while (true) {
			log.info("Getting total number of articles with offset: {}", pagesCounter * ARTICLES_PER_PAGE);
			Response response = given()
					.pathParam("section", ALL_SECTIONS.getValue())
					.pathParam("time-period", DAY.getValue())
					.param("offset", pagesCounter * ARTICLES_PER_PAGE)
					.get(MOSTEMAILED_ENDPOINT);

			response
					.then()
					.assertThat()
					.statusCode(200)
					.body("status", is("OK"));

			log.info("Response is {}", response.getStatusCode());
			log.debug("Response body is {}", response.body().toString());

			List<Article> articlesOnThisPage = response
					.getBody()
					.jsonPath()
					.getList("results", Article.class);

			if (!articlesOnThisPage.isEmpty()) {
				totalArticles.addAll(articlesOnThisPage);
				pagesCounter++;
			} else {
				log.info("Finished. Total number of retrieved articles: {}", totalArticles.size());
				break;
			}
		}
	}

	@Test(groups = {"P0", "Smoke", "Regression", "Filter", "Most Mailed"}, description = "Get number of articles per section",
			dependsOnMethods = "getMostMailedArticlesTest")
	public void countArticlesNumberTest() throws Exception {
		log.info("Step 2: Count articles number of each section");

		if (log.isDebugEnabled()) {
			for (Article article : totalArticles) {
				// it's needed to catch unknown sections
				if (article.getSection() == null) {
					log.error("Unknown section detected with url: {}, title: {}", article.getUrl(), article.getTitle());
				}
			}
		}
		// Calculating number of articles per each category
		articlesPerSection = totalArticles
				.stream()
				.collect(Collectors.groupingBy(Article::getSection, Collectors.counting()));
		for (Map.Entry<Section, Long> entry : articlesPerSection.entrySet()) {
			log.info("The number of articles for '{}' is: {}", entry.getKey(), entry.getValue());
		}

		log.info("Step 3: Get the list of sections from response.");
		// Getting the set of sections
		sections = articlesPerSection.keySet();
		log.info("Total number of sections under test is {}", sections.size());

		Assert.assertNotNull(sections, "No sections found");
	}

	@DataProvider(name = "Retrieved Sections")
	public static Object[][] retrievedSections() {
		return sections
				.stream()
				.map(section -> new Object[]{section})
				.toArray(Object[][]::new);
	}

	@Test(groups = {"P0", "Smoke", "Regression", "Filter", "Most Mailed"}, description = "Verify filtering per each section",
			dependsOnMethods = {"getMostMailedArticlesTest", "countArticlesNumberTest"}, dataProvider = "Retrieved Sections")
	public void checkSectionsFilteringTest(Section section) throws Exception {
		log.info("Step 4: Verify each section filtering");
		int pagesCounter = 0;
		List<Article> articlesForSection = new ArrayList<>();
		while (true) {
			Response response = given()
					.pathParam("section", section.getValue())
					.pathParam("time-period", DAY.getValue())
					.param("offset", pagesCounter * ARTICLES_PER_PAGE)
					.get(MOSTEMAILED_ENDPOINT);

			log.info("Getting total number of articles with offset: {} for section: {}",
					pagesCounter * ARTICLES_PER_PAGE, section.getValue());
			response
					.then()
					.assertThat()
					.statusCode(200);

			log.info("Response is {}", response.getStatusCode());
			log.debug("Response body is {}", response.body().toString());

			response.getBody().jsonPath().getInt("num_results");
			List<Article> articlesOnThisPage = response
					.getBody()
					.jsonPath()
					.getList("results", Article.class);

			if (!articlesOnThisPage.isEmpty()) {
				articlesForSection.addAll(articlesOnThisPage);
				pagesCounter++;
			} else {
				log.info("Finished. Total number of retrieved articles: {}", articlesForSection.size());
				break;
			}
		}

		log.info("Comparing section for all articles: {}", section);
		long numberOrArticles = 0;
		for (Article article : articlesForSection) {
			Assert.assertEquals(article.getSection(), section, "Sections don't match!");
			numberOrArticles++;
		}

		log.info("Comparing number of articles: {}", section);
		Assert.assertEquals(numberOrArticles, (long) articlesPerSection.get(section),
				"Number of article differs!");
	}
}

package com.ring.mostmailed;

import com.ring.BaseApiTest;
import dto.Article;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static enums.Section.ALL_SECTIONS;
import static enums.TimePeriod.DAY;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class NumResultsTests extends BaseApiTest {

	private final Logger log = LoggerFactory.getLogger(NumResultsTests.class);
	private List<Article> totalArticles = new ArrayList<>();

	@Test(groups = {"P0", "Smoke", "Regression", "Most Mailed"}, testName = "Test for 'num_results' value",
			description = "Verify 'num_results' value")
	public void numResultsTest() throws Exception {
		int pagesCounter = 0;
		int numResultsOnFirstPage = 0;
		int numResults = 0;
		while (true) {
			RequestSpecification request = given()
					.pathParam("section", ALL_SECTIONS.getValue())
					.pathParam("time-period", DAY.getValue())
					.param("offset", pagesCounter * ARTICLES_PER_PAGE);

			log.info("Getting total number of articles");
			Response response = request.get(MOSTEMAILED_ENDPOINT);
			response
					.then()
					.assertThat()
					.statusCode(200)
					.body("status", is("OK"));

			log.info("Response is {}", response.getStatusCode());
			log.debug("Response body is {}", response.body().toString());

			List<Article> articlesOnThisPage = response
					.getBody()
					.jsonPath()
					.getList("results", Article.class);

			if (!articlesOnThisPage.isEmpty()) {
				totalArticles.addAll(articlesOnThisPage);
				if (pagesCounter == 0) {
					// storing initial number of results
					numResultsOnFirstPage = response.getBody().jsonPath().getInt("num_results");
				} else {
					// verifying if number of results changes during paginating
					numResults = response.getBody().jsonPath().getInt("num_results");
					Assert.assertEquals(numResults, numResultsOnFirstPage, "Num results value changed during paginating");
				}
				pagesCounter++;
			} else {
				break;
			}
		}

		int retrievedArticlesSize = totalArticles.size();
		Assert.assertEquals(retrievedArticlesSize, numResults,
				"Size of list with retrieved articles doesn't match 'num_results' from JSON");
	}
}

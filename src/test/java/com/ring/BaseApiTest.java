package com.ring;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import support.reports.ExtentReportsListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Listeners(ExtentReportsListener.class)
public abstract class BaseApiTest {

	private static Logger logger = LoggerFactory.getLogger(BaseApiTest.class);
	protected static final Properties CONFIG;
	protected static final String API_KEY;

	protected static final int ARTICLES_PER_PAGE = 20; // it's default value according to Times API Pagination

	// Endpoints
	protected static final String MOSTEMAILED_ENDPOINT = "/mostemailed/{section}/{time-period}.json";
	protected static final String MOSTSHARED_ENDPOINT = "/mostshared/{section}/{time-period}.json";
	protected static final String MOSTVIEWED_ENDPOINT = "/mostviewed/{section}/{time-period}.json";
	protected static final String SECTIONS_ENDPOINT = "/viewed/sections.json";


	static {
		CONFIG = new Properties();
		String currentDir = System.getProperty("user.dir");
		File apiProps = new File(currentDir + "/src/main/resources/nyapi.properties");
		try {
			CONFIG.load(new FileInputStream(apiProps));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			API_KEY = CONFIG.getProperty("api.key");
		}
	}

	@BeforeTest
	public void setUpEnvironment() throws Exception {
		logger.info("Setting up environment for tests");

		// setting base host
		String host = System.getProperty("server.host", "http://api.nytimes.com/");
		RestAssured.baseURI = host.contains("local") ? "http://localhost" : host;
		logger.info("Server host is {}", RestAssured.baseURI);

		// setting base path
		String version = System.getProperty("version", "v2");
		RestAssured.basePath = System.getProperty("server.base", String.format("svc/mostpopular/%s", version));
		logger.info("Api version under test is {}", version);

		RestAssured.requestSpecification = new RequestSpecBuilder()
				.addParam("api-key", API_KEY)
				.build();
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

	}
}
